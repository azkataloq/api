Django==3.0.7
Pillow==7.1.2
django-modeltranslation==0.15
djangorestframework==3.11.0
django-filter==2.3.0
names
python-lorem
