from django.apps import AppConfig


class FiltersConfig(AppConfig):
    name = 'search_filters'
