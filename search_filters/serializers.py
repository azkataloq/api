"""Limit filter options based on existing filters."""
from rest_framework import serializers


class CategoryFilterSerializer(serializers.Serializer):
    slug = serializers.SlugField()
    name = serializers.CharField()
    count = serializers.IntegerField(default=0)


class SubCategoryFilterSerializer(serializers.Serializer):
    slug = serializers.SlugField()
    name = serializers.CharField()
    count = serializers.IntegerField(default=0)


class AuthorFilterSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    full_name = serializers.CharField()
    count = serializers.IntegerField(default=0)


class SourceFilterSerializer(serializers.Serializer):
    slug = serializers.SlugField()
    name = serializers.CharField()
    count = serializers.IntegerField(default=0)


class CopyrightOwnerFilterSerializer(serializers.Serializer):
    slug = serializers.SlugField()
    name = serializers.CharField()
    count = serializers.IntegerField(default=0)


class FilterSerializer(serializers.Serializer):
    """
    Main response showing all related filter options.

    Number of options per filter is limited by MAX_RESULTS_PER_FILTER settings.
    """
    total_content = serializers.IntegerField(default=0)
    start_year = serializers.IntegerField(allow_null=True)
    end_year = serializers.IntegerField(allow_null=True)
    categories = CategoryFilterSerializer(many=True)
    sub_categories = SubCategoryFilterSerializer(many=True)
    authors = AuthorFilterSerializer(many=True)
    sources = SourceFilterSerializer(many=True)
    copyright_owners = CopyrightOwnerFilterSerializer(many=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass
