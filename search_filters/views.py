from django.conf import settings
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListAPIView
from rest_framework.response import Response

from content.filters import ContentFilter
from content.models import CategoryChoices, SubCategoryChoices
from content.models import Content, Author, CopyrightOwner
from location.models import ContentSource
from search_filters.serializers import FilterSerializer


class SearchFilterView(ListAPIView):
    queryset = Content.objects.all()
    serializer_class = FilterSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_class = ContentFilter

    def list(self, request, *args, **kwargs):
        contents = self.filter_queryset(self.get_queryset())
        categories = [
            {
                "slug": slug, "name": name, "count": contents.filter(category=slug).count()
            } for slug, name in CategoryChoices.choices
        ]
        sub_categories = [
            {
                "slug": slug, "name": name, "count": contents.filter(subcategory=slug).count()
            } for slug, name in SubCategoryChoices.choices
        ]
        response_data = {
            "categories": categories, "sub_categories": sub_categories, "total_content": contents.count(),
            "start_year": contents.values_list("date__year", flat=True).order_by("date").first(),
            "end_year": contents.values_list("date__year", flat=True).order_by("-date").first(),
            "authors": [
                {
                    "pk": author.pk, "full_name": author.full_name, "count": contents.filter(authors=author).count()
                } for author in Author.objects.filter(
                    pk__in=contents.values_list("authors__pk", flat=True).order_by(
                        "authors__last_name"
                    ).distinct()[:settings.MAX_RESULTS_PER_FILTER]
                )
            ],
            "sources": [
                {
                    "slug": source.slug, "name": source.name, "count": contents.filter(source=source).count()
                } for source in ContentSource.objects.filter(
                    pk__in=contents.values_list("source__pk", flat=True).order_by(
                        "source__name"
                    ).distinct()[:settings.MAX_RESULTS_PER_FILTER]
                )
            ],
            "copyright_owners": [
                {
                    "slug": owner.slug, "name": owner.name,
                    "count": contents.filter(copyright_owners=owner).count()
                } for owner in CopyrightOwner.objects.filter(
                    pk__in=contents.values_list("copyright_owners__pk", flat=True).order_by(
                        "copyright_owners__last_name"
                    ).distinct()[:settings.MAX_RESULTS_PER_FILTER]
                )
            ]
        }
        response = FilterSerializer(data=response_data)
        assert response.is_valid(), response.errors
        return Response(response.data)
