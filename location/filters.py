from django_filters.rest_framework import FilterSet

from location.models import Location, ContentSource


class LocationFilter(FilterSet):
    class Meta:
        model = Location
        fields = ["country", "country_code", "city", "district", "neighborhood", "postal_code"]


class ContentSourceFilter(FilterSet):
    class Meta:
        model = ContentSource
        fields = ["location__slug"]
