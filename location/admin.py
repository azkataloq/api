from django.contrib import admin
from location.models import Location, ContentSource


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = ("title", "postal_code", "neighborhood", "district", "city", "country_code")
    list_filter = ("country_code",)
    search_fields = ("title", "postal_code")
    prepopulated_fields = {"slug": ("title",)}


@admin.register(ContentSource)
class ContentSourceAdmin(admin.ModelAdmin):
    list_display = ("name", "location")
    list_filter = ("location__country_code",)
    search_fields = ("name", "location__postal_code")
    prepopulated_fields = {"slug": ("name",)}
