from django.db import models
from django.utils.translation import ugettext_lazy as _

from azkataloq.utils import create_unique_slug


class Location(models.Model):
    title = models.CharField(_("Display title"), max_length=255)
    slug = models.SlugField(_("Slug"), max_length=50, unique=True)
    country = models.CharField(_("Country"), max_length=100, blank=True, null=True)
    country_code = models.CharField(_("Country Code"), max_length=5, blank=True, null=True)
    city = models.CharField(_("City"), max_length=100, blank=True, null=True)
    district = models.CharField(_("District"), max_length=100, null=True, blank=True)
    neighborhood = models.CharField(_("Neighborhood"), max_length=100, null=True, blank=True)
    postal_code = models.CharField(_("Postal Code"), max_length=25, null=True, blank=True)

    class Meta:
        verbose_name = _("Location")
        verbose_name_plural = _("Locations")
        ordering = ("title",)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = create_unique_slug(Location, self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title


class ContentSource(models.Model):
    name = models.CharField(_("Name"), max_length=255)
    slug = models.SlugField(_("Slug"), max_length=50, unique=True)
    location = models.ForeignKey(
        Location, verbose_name=_("Location"), on_delete=models.SET_NULL, related_name="sources",
        blank=True, null=True
    )

    class Meta:
        verbose_name = _("Content source")
        verbose_name_plural = _("Content sources")
        ordering = ("name",)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = create_unique_slug(ContentSource, self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name
