from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import SearchFilter, OrderingFilter

from location.filters import LocationFilter, ContentSourceFilter
from location.models import Location, ContentSource
from location.serializers import LocationSerializer, ContentSourceSerializer


class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter,)
    filter_class = LocationFilter
    lookup_field = "slug"
    search_fields = ("title", "postal_code")
    ordering_fields = ("title", "city", "postal_code")


class ContentSourceViewSet(viewsets.ModelViewSet):
    queryset = ContentSource.objects.all()
    serializer_class = ContentSourceSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter,)
    filter_class = ContentSourceFilter
    lookup_field = "slug"
    search_fields = ("name",)
    ordering_fields = ("name",)
