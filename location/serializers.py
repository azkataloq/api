from rest_framework import serializers

from location.models import Location, ContentSource


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ["title", "slug", "country", "country_code", "city", "district", "neighborhood", "postal_code"]
        lookup_field = "slug"
        extra_kwargs = {
            "url": {"lookup_field": "slug"}
        }


class ContentSourceSerializer(serializers.ModelSerializer):
    location = LocationSerializer(read_only=True)

    class Meta:
        model = ContentSource
        fields = ["name", "slug", "location"]
        lookup_field = "slug"
        extra_kwargs = {
            "url": {"lookup_field": "slug"}
        }
