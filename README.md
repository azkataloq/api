# AzKataloq API

Backend API for the AzKataloq front-end

# Install

- Create a virtual environment: `virtualenv venv -p python3`
- Activate the virtual environment: `source venv/bin/activate`
- Install the project requirements: `pip install -r requirements.txt`
- Migrate the database: `python manage.py migrate`
- Create random data for testing: `python manage.py generate_example_data --contents 100`
- Create an admin user: `python manage.py createsuperuser`
- Run the development site: `python manage.py runserver`

# Custom settings

- To use custom settings, create the local settings file: `cp local_settings.py.template local_settings.py`
- Modify its contents and overwrite your needs.
- This file is only for local usage, it is not committed to the project repository.
