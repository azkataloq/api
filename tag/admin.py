from django.contrib import admin
from tag.models import Tag


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ("name", "content_count")
    search_fields = ("name",)
    prepopulated_fields = {"slug": ("name",)}
