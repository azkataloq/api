from rest_framework import serializers

from tag.models import Tag


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ["name", "slug", "content_count"]
        lookup_field = "slug"
        extra_kwargs = {
            "url": {"lookup_field": "slug"}
        }
