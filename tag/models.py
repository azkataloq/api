from django.db import models
from django.utils.translation import ugettext_lazy as _

from azkataloq.utils import create_unique_slug


class Tag(models.Model):
    name = models.CharField(_("Name"), max_length=150)
    slug = models.SlugField(_("Slug"), max_length=50, unique=True)

    class Meta:
        verbose_name = _("Tag")
        verbose_name_plural = _("Tags")
        ordering = ("name",)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = create_unique_slug(Tag, self.name)
        super().save(*args, **kwargs)

    @property
    def content_count(self):
        return self.items.count()

    def __str__(self):
        return self.name
