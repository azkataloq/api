from rest_framework import viewsets
from rest_framework.filters import SearchFilter, OrderingFilter

from tag.models import Tag
from tag.serializers import TagSerializer


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    filter_backends = (OrderingFilter, SearchFilter,)
    lookup_field = "slug"
    search_fields = ("name",)
    ordering_fields = ("name",)
