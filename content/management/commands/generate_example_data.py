import random
import string
from datetime import datetime
from logging import getLogger

import lorem
import names
from django.core.management.base import BaseCommand

from content.models import (
    Collection, CopyrightOwner, Author, StatusChoices, CATEGORY_DEPENDENCY, CategoryChoices,
    Content
)
from location.models import Location, ContentSource
from tag.models import Tag
from user.models import User

logger = getLogger('django')


def get_random_date(start: int = None, end: int = None):
    if not start:
        start = int(datetime(year=1650, month=1, day=1, hour=5).timestamp())
    if not end:
        end = int(datetime(year=2000, month=1, day=30, hour=5).timestamp())
    result = datetime.fromtimestamp(random.randint(start, end))
    return result.date()


def create_tags():
    for name in ["book", "photo", "negative", "handmade", "music", "historical", "documentary"]:
        Tag.objects.get_or_create(name=name)


def create_users():
    if not User.objects.filter(is_superuser=True).exists():
        User.objects.create_superuser(
            username="admin", password="admin", email="admin@test.em", first_name="Admin", last_name="User"
        )
        logger.info("Admin user created with credentials admin/admin.")
    if not User.objects.filter(is_superuser=False, is_staff=False).exists():
        User.objects.create_user(
            username="tester", password="tester", email="tester@test.em", first_name="Dummy", last_name="Tester"
        )
        logger.info("Test user created with credentials tester/tester.")


# noinspection SpellCheckingInspection
def create_locations():
    counter = 0
    country = "Azərbaycan"
    country_code = "AZ"
    for city in ["Bakı", "Gəncə"]:
        if city == "Bakı":
            districts = ["Xətai", "Nəsimi", "Səbail", "Nizami", "Xəzər"]
            postal_index = "AZ10{index}"
            index_counter = 10
        else:
            districts = ["Hacıkənd", "Cavadxan", "Natavan"]
            postal_index = "AZ20{index}"
            index_counter = 10
        for district in districts:
            for i in range(1, 4):
                neighborhood = f"Neighborhood {i}"
                postal_code = postal_index.format(index=str(index_counter))
                Location.objects.create(
                    country=country, country_code=country_code, city=city, district=district,
                    neighborhood=neighborhood, postal_code=postal_code,
                    title=f"{postal_code}, {neighborhood}, {district}, {city}, {country_code}"
                )
                counter += 1
                index_counter += 1
    logger.info(f"{counter} locations created.")


def create_content_sources():
    counter = 0
    limit = Location.objects.count()
    for i in range(1, 10):
        letter = random.choice(string.ascii_uppercase)
        name = f"{letter} Museum"
        location = Location.objects.all()[random.randint(0, limit - 1)]
        ContentSource.objects.create(name=name, location=location)
        counter += 1
    logger.info(f"{counter} content sources created.")


def create_authors():
    counter = 0
    for i in range(0, 50):
        birthday = get_random_date()
        last_name = names.get_last_name()
        first_name = names.get_first_name(gender=random.choice(["male", "female"]))
        Author.objects.create(last_name=last_name, first_name=first_name, birthday=birthday)
        counter += 1
    logger.info(f"{counter} authors created.")


def create_copyright_owners():
    counter = 0
    for i in range(0, 15):
        CopyrightOwner.objects.create(name=names.get_full_name(gender=random.choice(["male", "female"])))
        counter += 1
    logger.info(f"{counter} copyright owners created.")


def create_collections():
    counter = 0
    for i in range(0, 30):
        name = names.get_last_name()
        location = Location.objects.all()[random.randint(0, Location.objects.count() - 1)]
        Collection.objects.create(name=name, location=location)
        counter += 1
    logger.info(f"{counter} collections created.")


def create_contents(content_count: int):
    counter = 0
    for i in range(content_count):
        name = names.get_full_name(gender=random.choice(["male", "female"]))
        description = lorem.get_paragraph(count=3, comma=(0, 2), word_range=(4, 8), sentence_range=(5, 10))
        short_description = description.split("\n")[0]
        category = random.choice(CategoryChoices.values)
        sub_category = random.choice(CATEGORY_DEPENDENCY[category]).value
        collection = Collection.objects.all()[random.randint(0, Collection.objects.count() - 1)]
        location = Location.objects.all()[random.randint(0, Location.objects.count() - 1)]
        source = ContentSource.objects.all()[random.randint(0, ContentSource.objects.count() - 1)]
        copyright_owner = CopyrightOwner.objects.all()[random.randint(0, CopyrightOwner.objects.count() - 1)]
        isbn = random.randint(1000000000000, 9000000000000)
        while Content.objects.filter(isbn=str(isbn)).exists():
            isbn = random.randint(1000000000000, 9000000000000)
        if random.choice([True, False]):
            dimensions = "{value} sm".format(value=random.choice(["1920x1080", "1366x768", "1280x720", "1024x768"]))
        else:
            dimensions = None
        content = Content.objects.create(
            title=name, description=description, short_description=short_description, category=category,
            subcategory=sub_category, collection=collection, location=location, source=source,
            date=get_random_date(), isbn=isbn,
            inventory_number=random.randint(50, 999), dimensions=dimensions, status=StatusChoices.PUBLIC
        )
        content.copyright_owners.add(copyright_owner)
        counter += 1
        for j in range(0, random.randint(1, 3)):
            author = Author.objects.all()[random.randint(0, Author.objects.count() - 1)]
            content.authors.add(author)
        for j in range(0, random.randint(1, 2)):
            digital_author = Author.objects.all()[random.randint(0, Author.objects.count() - 1)]
            content.digital_authors.add(digital_author)
        for j in range(0, random.randint(0, 4)):
            tag_qs = Tag.objects.exclude(id__in=content.tags.values_list("id", flat=True))
            tag = tag_qs[random.randint(0, tag_qs.count() - 1)]
            content.tags.add(tag)
    logger.info(f"{counter} contents created.")


def create_base_data(content_count: int = 25):
    create_tags()
    create_users()
    create_locations()
    create_content_sources()
    create_authors()
    create_copyright_owners()
    create_collections()
    create_contents(content_count)


class Command(BaseCommand):
    help = "Populate database with necessary data for testing."

    def add_arguments(self, parser):
        parser.add_argument('--contents', type=int)

    def handle(self, *args, **options):
        contents = options.get("contents")
        if not contents:
            contents = 25
        create_base_data(contents)
