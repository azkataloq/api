from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as ug

from azkataloq.utils import create_unique_slug
from location.models import Location, ContentSource
from tag.models import Tag


def get_content_image_path(instance, filename):
    extension = filename.split(".")[-1]
    file_name = instance.slug
    year = timezone.now().year
    return f"contents/{year}/{file_name}.{extension}"


class CategoryChoices(models.TextChoices):
    PHOTO = "photo", ug("Photos and negatives")
    PUBLICATION = "publication", ug("Books, newspapers and journals")
    IMAGE = "graphic", ug("Pictures and Graphics")


class SubCategoryChoices(models.TextChoices):
    PHOTO = "photo", ug("Photo")
    NEGATIVE = "negative", ug("Negative")
    BOOK = "book", ug("Book")
    NEWSPAPER = "newspaper", ug("Newspaper")
    JOURNAL = "journal", ug("Journal")
    IMAGE = "image", ug("Picture")
    GRAPHIC = "graphic", ug("Graphic")


CATEGORY_DEPENDENCY = {
    CategoryChoices.PHOTO: [SubCategoryChoices.PHOTO, SubCategoryChoices.NEGATIVE],
    CategoryChoices.PUBLICATION: [SubCategoryChoices.BOOK, SubCategoryChoices.NEWSPAPER, SubCategoryChoices.JOURNAL],
    CategoryChoices.IMAGE: [SubCategoryChoices.IMAGE, SubCategoryChoices.GRAPHIC]
}


class StatusChoices(models.TextChoices):
    DRAFT = "draft", _("Draft")
    PUBLIC = "public", _("Public")
    HIDDEN = "hidden", _("Hidden")


class Author(models.Model):
    last_name = models.CharField(_("Last name"), max_length=150)
    first_name = models.CharField(_("First name"), max_length=150, blank=True, null=True)
    birthday = models.DateField(_("Birthday"), blank=True, null=True)

    class Meta:
        verbose_name = _("Author")
        verbose_name_plural = _("Authors")
        ordering = ("last_name", "first_name")

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()


class CopyrightOwner(models.Model):
    name = models.CharField(_("Name"), max_length=255)
    slug = models.SlugField(_("Slug"), max_length=50, unique=True)

    class Meta:
        verbose_name = _("Copyright owner")
        verbose_name_plural = _("Copyright owners")
        ordering = ("name",)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = create_unique_slug(CopyrightOwner, self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Collection(models.Model):
    name = models.CharField(_("Name"), max_length=255)
    slug = models.SlugField(_("Slug"), max_length=50, unique=True)
    location = models.ForeignKey(
        Location, verbose_name=_("Location"), on_delete=models.SET_NULL, related_name="collections",
        blank=True, null=True
    )

    class Meta:
        verbose_name = _("Collection")
        verbose_name_plural = _("Collections")
        ordering = ("name",)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = create_unique_slug(Collection, self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Content(models.Model):
    title = models.CharField(_("Title"), max_length=255)
    slug = models.SlugField(_("Slug"), max_length=50, unique=True)
    description = models.TextField(_("Description"), default="", blank=True, null=True)
    short_description = models.TextField(_("Short Description"), max_length=255, blank=True, null=True)
    category = models.CharField(
        _("Category"), max_length=50, choices=CategoryChoices.choices, default=CategoryChoices.PHOTO
    )
    subcategory = models.CharField(
        _("Subcategory"), max_length=50, choices=SubCategoryChoices.choices, default=SubCategoryChoices.PHOTO
    )
    image = models.ImageField(_("Image"), upload_to=get_content_image_path)
    collection = models.ForeignKey(
        Collection, verbose_name=_("Collection"), on_delete=models.SET_NULL, related_name="items",
        blank=True, null=True
    )
    location = models.ForeignKey(
        Location, verbose_name=_("Location"), on_delete=models.SET_NULL, related_name="items", blank=True, null=True
    )
    source = models.ForeignKey(
        ContentSource, verbose_name=_("Content source"), on_delete=models.SET_NULL, related_name="items",
        blank=True, null=True
    )
    date = models.DateField(_("Date"), blank=True, null=True)
    # owners
    authors = models.ManyToManyField(
        Author, verbose_name=_("Authors"), related_name="originals", blank=True
    )
    digital_authors = models.ManyToManyField(
        Author, verbose_name=_("Digital authors"), related_name="items", blank=True
    )
    copyright_owners = models.ManyToManyField(
        CopyrightOwner, verbose_name=_("Copyright owners"), related_name="items", blank=True
    )
    isbn = models.CharField(_("ISBN"), max_length=150, blank=True, null=True)
    inventory_number = models.CharField(_("Inventory number"), max_length=200, blank=True, null=True)
    doi = models.CharField(_("DOI"), max_length=150, blank=True, null=True)
    dimensions = models.CharField(_("Dimensions"), max_length=150, blank=True, null=True)
    status = models.CharField(_("Status"), max_length=50, choices=StatusChoices.choices, default=StatusChoices.DRAFT)
    tags = models.ManyToManyField(Tag, verbose_name=_("Tags"), related_name="items", blank=True)
    created_at = models.DateTimeField(_("Created"), auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(_("Modified"), auto_now=True, editable=False)

    class Meta:
        verbose_name = _("Content")
        verbose_name_plural = _("Contents")
        ordering = ("-modified_at",)

    def save(self, *args, **kwargs):
        if not self.slug and self.title:
            self.slug = create_unique_slug(Content, self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title
