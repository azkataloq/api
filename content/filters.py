from django.utils.translation import ugettext_lazy as _
from django_filters.rest_framework import (
    FilterSet, DateTimeFromToRangeFilter, NumberFilter, ModelMultipleChoiceFilter, MultipleChoiceFilter
)

from content.models import Content, CategoryChoices, SubCategoryChoices, Author, Collection, CopyrightOwner


class ContentFilter(FilterSet):
    category = MultipleChoiceFilter(field_name="category", choices=CategoryChoices.choices)
    subcategory = MultipleChoiceFilter(field_name="subcategory", choices=SubCategoryChoices.choices)
    date = DateTimeFromToRangeFilter(field_name="date")
    year_from = NumberFilter(label=_("Year from"), method="filter_year_from")
    year_to = NumberFilter(label=_("Year to"), method="filter_year_to")
    authors = ModelMultipleChoiceFilter(
        field_name="authors__id", to_field_name="id", label=_("Authors"),
        queryset=Author.objects.all()
    )
    digital_authors = ModelMultipleChoiceFilter(
        field_name="digital_authors__id", to_field_name="id", label=_("Digital authors"),
        queryset=Author.objects.all()
    )
    copyright_owners = ModelMultipleChoiceFilter(
        field_name="copyright_owners__slug", to_field_name="slug", label=_("Copyright owners"),
        queryset=CopyrightOwner.objects.all()
    )

    @staticmethod
    def filter_year_from(queryset, name, value):
        if value:
            return queryset.filter(**{"date__year__gte": value})
        return queryset

    @staticmethod
    def filter_year_to(queryset, name, value):
        if value:
            return queryset.filter(**{"date__year__lte": value})
        return queryset

    class Meta:
        model = Content
        fields = [
            "category", "subcategory", "collection__slug", "location__slug", "source__slug", "year_from",
            "year_to", "date", "authors", "digital_authors", "copyright_owners", "tags__slug"
        ]


class AuthorFilter(FilterSet):
    birthday = DateTimeFromToRangeFilter(field_name="birthday")

    class Meta:
        model = Author
        fields = ["birthday"]


class CollectionFilter(FilterSet):
    class Meta:
        model = Collection
        fields = ["location__slug"]
