from rest_framework import serializers

from content.models import Content, Author, Collection, CopyrightOwner
from location.serializers import LocationSerializer, ContentSourceSerializer
from tag.serializers import TagSerializer


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ["pk", "last_name", "first_name", "birthday"]
        lookup_field = "pk"
        extra_kwargs = {
            "url": {"lookup_field": "pk"}
        }


class CollectionSerializer(serializers.ModelSerializer):
    location = LocationSerializer(read_only=True)

    class Meta:
        model = Collection
        fields = ["name", "slug", "location"]
        lookup_field = "slug"
        extra_kwargs = {
            "url": {"lookup_field": "slug"}
        }


class CopyrightOwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = CopyrightOwner
        fields = ["name", "slug"]
        lookup_field = "slug"
        extra_kwargs = {
            "url": {"lookup_field": "slug"}
        }


class ContentSerializer(serializers.ModelSerializer):
    authors = AuthorSerializer(read_only=True, many=True)
    digital_authors = AuthorSerializer(read_only=True, many=True)
    collection = CollectionSerializer(read_only=True)
    copyright_owners = CopyrightOwnerSerializer(read_only=True, many=True)
    location = LocationSerializer(read_only=True)
    source = ContentSourceSerializer(read_only=True)
    tags = TagSerializer(read_only=True, many=True)

    class Meta:
        model = Content
        fields = [
            "url", "title", "slug", "description", "short_description", "category", "subcategory", "image",
            "collection", "location", "source", "date", "authors", "digital_authors", "copyright_owners", "isbn",
            "inventory_number", "doi", "dimensions", "tags", "created_at", "modified_at"
        ]
        lookup_field = "slug"
        extra_kwargs = {
            "url": {"lookup_field": "slug"}
        }

    def get_fields(self, *args, **kwargs):
        fields = super().get_fields()
        request = self.context.get('request')
        if request is not None and not request.parser_context.get('kwargs'):
            fields.pop('description', None)
        return fields
