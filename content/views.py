from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import SearchFilter, OrderingFilter

from content.filters import ContentFilter, AuthorFilter, CollectionFilter
from content.models import Content, StatusChoices, Author, Collection, CopyrightOwner
from content.serializers import (
    ContentSerializer, AuthorSerializer, CollectionSerializer, CopyrightOwnerSerializer
)


class ContentViewSet(viewsets.ModelViewSet):
    queryset = Content.objects.all()
    serializer_class = ContentSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter,)
    filter_class = ContentFilter
    lookup_field = "slug"
    search_fields = ("title", "isbn", "inventory_number", "doi", "tags__name")
    ordering_fields = ("modified_at", "created_at", "date")

    def get_queryset(self):
        if self.request.user.is_staff:
            return Content.objects.all()
        return Content.objects.filter(status=StatusChoices.PUBLIC)


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter,)
    filter_class = AuthorFilter
    lookup_field = "pk"
    search_fields = ("last_name", "first_name",)
    ordering_fields = ("last_name",)


class CollectionViewSet(viewsets.ModelViewSet):
    queryset = Collection.objects.all()
    serializer_class = CollectionSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter,)
    filter_class = CollectionFilter
    lookup_field = "slug"
    search_fields = ("name",)
    ordering_fields = ("name",)


class CopyrightOwnerViewSet(viewsets.ModelViewSet):
    queryset = CopyrightOwner.objects.all()
    serializer_class = CopyrightOwnerSerializer
    filter_backends = (OrderingFilter, SearchFilter,)
    lookup_field = "slug"
    search_fields = ("name",)
    ordering_fields = ("name",)
