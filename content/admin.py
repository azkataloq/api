from django.contrib import admin
from content.models import Author, CopyrightOwner, Collection, Content
from modeltranslation.admin import TranslationAdmin


@admin.register(Content)
class ContentAdmin(TranslationAdmin):
    list_display = (
        "title", "category", "subcategory", "collection", "date", "location", "source", "status", "modified_at"
    )
    list_filter = ("modified_at", "category", "subcategory")
    search_fields = ("title",)
    autocomplete_fields = (
        "collection", "location", "source", "authors", "digital_authors", "copyright_owners", "tags"
    )
    prepopulated_fields = {"slug": ("title",)}


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ("last_name", "first_name", "birthday")
    list_filter = ("birthday",)
    search_fields = ("first_name", "last_name")


@admin.register(CopyrightOwner)
class CopyrightOwnerAdmin(admin.ModelAdmin):
    list_display = ("name",)
    search_fields = ("name",)


@admin.register(Collection)
class CollectionAdmin(admin.ModelAdmin):
    list_display = ("name", "location")
    search_fields = ("name",)
    autocomplete_fields = ("location",)
