from django.conf import settings
from django.utils.text import slugify
from rest_framework.pagination import PageNumberPagination


class APIPagination(PageNumberPagination):
    page_size = getattr(settings, 'REST_FRAMEWORK', {}).get('PAGE_SIZE', 100)
    page_size_query_param = 'page_size'
    max_page_size = 1000


def create_unique_slug(obj, value) -> str:
    slug = slugify(value[:45])
    if obj.objects.filter(slug=slug).exists():
        counter = 1
        while obj.objects.filter(slug=slug + str(counter)).exists():
            counter += 1
        slug += str(counter)
    return slug
