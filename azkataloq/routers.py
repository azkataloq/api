from rest_framework import routers

from content.views import ContentViewSet, AuthorViewSet, CollectionViewSet, CopyrightOwnerViewSet
from location.views import LocationViewSet, ContentSourceViewSet
from tag.views import TagViewSet

router = routers.DefaultRouter()
router.register(r"contents", ContentViewSet)
router.register(r"authors", AuthorViewSet)
router.register(r"collections", CollectionViewSet)
router.register(r"copyright_owners", CopyrightOwnerViewSet)
router.register(r"locations", LocationViewSet)
router.register(r"sources", ContentSourceViewSet)
router.register(r"tags", TagViewSet)
