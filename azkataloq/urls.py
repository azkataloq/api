from django.contrib import admin
from django.urls import path, include
from azkataloq.routers import router
from search_filters.views import SearchFilterView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('filters/', SearchFilterView.as_view()),
    path('', include(router.urls)),
]
